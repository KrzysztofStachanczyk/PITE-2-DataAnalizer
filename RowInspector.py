from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from ui_rowinspector import Ui_rowInspector

"""
Widget odpowiedzialny za wypisanie danych z konretnego wiersza 
"""


class RowInspector(QWidget):
    def __init__(self, base=None):
        QWidget.__init__(self, base)
        self.ui = Ui_rowInspector()
        self.ui.setupUi(self)
        self.data = None
        self.rowIndex = 0
        self.timerIntervalInSeconds = 1
        self.timer = QTimer()

        self.ui.nextButton.clicked.connect(self.onNextButton)
        self.ui.prevButton.clicked.connect(self.onPrevButton)
        self.ui.playButton.clicked.connect(self.onTimingStart)
        self.ui.stopButton.clicked.connect(self.onTimingStop)
        self.timer.timeout.connect(self.onTimerTick)
        self.ui.sampleNumberSpin.valueChanged.connect(self.loadFrame)

    """
    Ładuje próbkę o zadanym rozmiarze
    """

    def loadFrame(self, frameNumber):
        self.updateCurrentRow(frameNumber)

    """
    Slot wywoływany w przypadku wciśnięcia przycisku "start".
    Inicjalizuje timer o zadanej częstotliwości.
    """

    def onTimingStart(self):
        self.timer.stop()
        self.timerIntervalInSeconds = 1 / self.ui.spinBox.value()
        self.timer.start(1000 * self.timerIntervalInSeconds)

    """
    Slot wywoływany w przypadku minięcia wyznaczonego czasu
    Powoduje wyświetlenie kolejnej próbki
    """

    def onTimerTick(self):
        if (self.rowIndex + 1) < len(self.data["throttle-left"]):
            self.updateCurrentRow(self.rowIndex + 1)
        else:
            self.timer.stop()

    """
    Slot wywoływany w momencie zarządania zatrzymania timera.
    Jest wyzwalany przez przycisk "stop"
    """

    def onTimingStop(self):
        self.timer.stop()

    """
    Slot wywoływany w przypadku wciśnięcia klawisza "->"
    Wyświetla następną próbkę danych
    """

    def onNextButton(self):
        self.updateCurrentRow(self.rowIndex + 1)

    """
    Slot wywoływany w przypadku wciśnięcia klawisza "<-".
    Wyświetla wcześniejszą próbkę danych
    """

    def onPrevButton(self):
        self.updateCurrentRow(self.rowIndex - 1)

    """
    Ustawia  źródło danych
    Argumenty:
        - dane
    """

    def loadNewData(self, data):
        self.data = data
        self.ui.sampleNumberSpin.setMaximum(len(self.data["throttle-left"]))

    """
    Slot wywołujący aktualizacje widgetu
    """

    def updateCurrentRow(self, newIndex):
        if self.data is None:
            return

        if newIndex >= 0 and newIndex < len(self.data["throttle-left"]):
            self.rowIndex = newIndex
            dataRow = dict()
            for key in self.data:
                dataRow[key] = self.data[key][self.rowIndex]

                self.refreshEnginePowerAndMechanical(dataRow)
                self.refreshLocation(dataRow)
                self.refreshOrientation(dataRow)
                self.refreshAltAndSpeed(dataRow)
                self.refreshTime(dataRow)

    """
    Metoda aktualizująca część odpowiedzialną za moc silników i mechanikę
    Argumenty:
        - wiersz z danymi
    """

    def refreshEnginePowerAndMechanical(self, data):
        if "throttle-left" in data:
            self.ui.leftEngineProgress.setValue(data["throttle-left"] * 100)
        else:
            self.ui.leftEngineProgress.setValue(0)

        if "throttle-right" in data:
            self.ui.rightEngineProgress.setValue(data["throttle-right"] * 100)
        else:
            self.ui.rightEngineProgress.setValue(0)

        if "flap-pos-norm" in data:
            self.ui.flapsProgress.setValue(data["flap-pos-norm"] * 100)
        else:
            self.ui.flapsProgress.setValue(0)

        if "gear-down" in data:
            if data["gear-down"] == 1:
                self.ui.gearState.setText("down")
            else:
                self.ui.gearState.setText("up")
        else:
            self.ui.gearState.setText("unknown")

    """
    Metoda aktualizująca część odpowiedzialną za lokalizację samolotu
    Argumenty:
        - wiersz z danymi
    """

    def refreshLocation(self, data):
        if "latitude-deg" in data:
            self.ui.latitudeDegree.setText("{0:.4f}".format(data["latitude-deg"]))
        else:
            self.ui.latitudeDegree.setText("unknown")

        if "longitude-deg" in data:
            self.ui.longitudeDegree.setText("{0:.4f}".format(data["longitude-deg"]))
        else:
            self.ui.longitudeDegree.setText("unknown")

    """
    Metoda aktualizująca część odpowiedzialną za orientację samolotu w przestrzeni
    Argumenty:
        - wiersz z danymi
    """

    def refreshOrientation(self, data):
        if "roll-deg" in data:
            self.ui.rollDegree.setText("{0:.2f}".format(data["roll-deg"]))
        else:
            self.ui.rollDegree.setText("unknown")

        if "heading-deg" in data:
            self.ui.headingDegree.setText("{0:.2f}".format(data["heading-deg"]))
        else:
            self.ui.headingDegree.setText("unknown")

        if "pitch-deg" in data:
            self.ui.pitchDegree.setText("{0:.2f}".format(data["pitch-deg"]))
        else:
            self.ui.pitchDegree.setText("unknown")

    """
    Metoda aktualizująca część odpowiedzialną za wysokość i prędkość samolotu
    Argumenty:
        - wiersz z danymi
    """

    def refreshAltAndSpeed(self, data):
        if "airspeed-kt" in data:
            self.ui.speedLCD.display((int)(data["airspeed-kt"] * 1.85200))
        else:
            self.ui.speedLCD.display(-1)

        if "altitude-to-ground-ft" in data:
            self.ui.altToGroundLCD.display((int)(data["altitude-to-ground-ft"] * 0.3048))
        else:
            self.ui.altToGroundLCD.display(-1)

        if "altitude-ft" in data:
            self.ui.altLCD.display((int)(data["altitude-ft"] * 0.3048))
        else:
            self.ui.altLCD.display(-1)

    """
    Metoda aktualizująca część odpowiedzialną za czas w którym przechwycono dane
    Argumenty:
        - wiersz z danymi
    """

    def refreshTime(self, data):
        if "time" in data:
            self.ui.timeLabel.setText((data["time"].strftime("%X %x")))
