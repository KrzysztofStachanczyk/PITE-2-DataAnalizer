from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import datetime
import os
import pickle

"""
Klasa umożliwiająca wczytanie danych z pliku o zadanej ścieżce
"""


class DataLoader(QObject):
    """
    Sygnał emitowany w przypadku udanego zakończenia wczytywania danych
    Argument:
        słownik zawierający listy odczytanych parametrów lotu
    """
    dataLoaded = pyqtSignal(dict)

    """
    Sygnał emitowany w przypadku wykrycia niekompletności wczytywanych danych
    lub napotkania błędów w procesie odczytu wartości z pliku
    """
    loadingError = pyqtSignal()

    def __init__(self, base=None):
        super(DataLoader, self).__init__(base)

        self.requiredKeys = [
            "throttle-left",
            "throttle-right",
            "latitude-deg",
            "longitude-deg",
            "altitude-ft",
            "altitude-to-ground-ft",
            "roll-deg",
            "pitch-deg",
            "heading-deg",
            "airspeed-kt",
            "flap-pos-norm",
            "gear-down",
            "brake-parking",
            "time"]

    """
    Wczytuje dane z pliku i emituje odpowiednie sygnały w zależności od tego czy odczyt się powiódł
    Argumenty:
        filePath - ścieżka do wczytywanego pliku
    """

    def loadNewFile(self, filePath):
        try:
            if filePath is None:
                return
            if os.path.isfile(filePath):

                file = open(filePath, 'rb')
                data = pickle.load(file)

                for key in self.requiredKeys:
                    if key not in data:
                        raise Exception()

                file.close()
            self.dataLoaded.emit(data)
        except:
            self.loadingError.emit()
