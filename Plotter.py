from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import *
from matplotlib.axes import *
from matplotlib.pyplot import *
from datetime import *
import matplotlib.dates as mdates
from matplotlib.dates import DateFormatter
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

"""
Klasa odpowiedzialna za rysowanie wykresów ze wsparciem wielu serii danych
"""


class Plotter(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=80):
        self.fig = Figure(figsize=(width, height), dpi=dpi)
        self.fig.autofmt_xdate()
        self.axes = self.fig.add_subplot(111)
        self.axes.hold(True)

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass

    """
    Aktualizuje dane na wykresie
    Przyjmuje argumenty:
        x - etykiety na osi poziomej będące listą zmiennych typu Data
        yList - serie danych zbudowane jako lista krotek zawierających:
            nazwę serii danych
            listę liczb należących do serii
            kolor wykresu dla zadanej serii (string w formacie np "b" "r" ... )
        xLabel - nazwa osi poziomej
        yLabel - nazwa osi pionowej
    """

    def updatePlot(self, x, yList, xLabel="", yLabel=""):
        self.fig.delaxes(self.axes)

        self.axes = self.fig.add_subplot(111)
        self.axes.hold(True)

        myFmt = mdates.DateFormatter('%H:%M:%S')
        self.axes.xaxis.set_major_formatter(myFmt)

        for element in yList:
            name, data, color = element
            self.axes.plot(x, data, color, label=name)

        self.axes.legend(loc=2, bbox_to_anchor=(0, 0.9), borderaxespad=0., fontsize='x-small', framealpha=0.25,
                         markerscale=0.5, ncol=1, numpoints=1)
        self.axes.set_xlabel(xLabel)
        self.axes.set_ylabel(yLabel)
        self.draw()
