import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from ui_mainwidget import Ui_mainWidget
from DataLoader import DataLoader
from numpy import *

"""
Główna klasa aplikacji odpowiedzialna za łączenie innych jej elementów w całość
i obsługę GUI
"""


class Application(QWidget):
    def __init__(self, base=None):
        QWidget.__init__(self, base)
        self.ui = Ui_mainWidget()
        self.ui.setupUi(self)
        self.dataLoader = DataLoader()

        # connect signals to slots
        self.ui.loadFileButton.clicked.connect(self.loadFileButtonIsClicked)
        self.dataLoader.loadingError.connect(self.onLoadingError)
        self.dataLoader.dataLoaded.connect(self.onDataLoaded)

    """
    Slot wywoływany w momencie gdy następuje wciśnięcie klawisza odpowiedzialnego
    za wczytanie nowego pliku z danymi. Wyświetla okno dialogowe wyboru pliku oraz
    po sprawdzeniu poprawności uzyskanych danych wywołuje operacje odpowiedzialne
    za odczyt danych
    """

    def loadFileButtonIsClicked(self):
        path, *otherStuff = QFileDialog.getOpenFileName()
        if path is not None:
            self.dataLoader.loadNewFile(path)

    """
    Slot wywoływany w sytuacji gdy nastąpił błąd odczytu danych z pliku.
    Wyświetla on komunikat o błędzie na ekranie.
    """

    def onLoadingError(self):
        QMessageBox.critical(self, "I/O Error", "Reading data from a file failed.", QMessageBox.Abort)

    """
    Slot wywoływany po otrzymaniu słownika zawierającego dane wczytane z pliku
    zarządzający aktualizacją elementów GUI
    """

    def onDataLoaded(self, data):
        self.ui.altChart.updatePlot(data["time"], [("altitude", array(data["altitude-ft"]) * 0.3048, "r"),
                                                   ("altitude to ground", array(data["altitude-to-ground-ft"]) * 0.3048,
                                                    "g"),
                                                   ("terrain height", array((data["altitude-ft"]) - array(
                                                       data["altitude-to-ground-ft"])) * 0.3048, "b")
                                                   ], "", "meters [m]")
        self.ui.speedChart.updatePlot(data["time"], [("air-speed", array(data["airspeed-kt"]) * 1.85200, "r")
                                                     ], "", "[km/h]")

        self.ui.rotationChart.updatePlot(data["time"], [("roll-deg", data["roll-deg"], "r"),
                                                        ("heading-deg", data["heading-deg"], "g"),
                                                        ("pitch-deg", data["pitch-deg"], "b")
                                                        ], "", "degree")

        self.ui.throttleAndFlapsChart.updatePlot(data["time"],
                                                 [("throttle-left", array(data["throttle-left"]) * 100, "r"),
                                                  ("throttle-right", array(data["throttle-right"]) * 100, "g"),
                                                  ("flap-pos-norm", array(data["flap-pos-norm"]) * 100, "b")
                                                  ], "", "percentage [%]")

        self.ui.gearChart.updatePlot(data["time"], [("gear-down", data["gear-down"], "y"),
                                                    ], "", "false-true (0-1)")

        self.ui.breakeChart.updatePlot(data["time"], [("brake-parking", data["brake-parking"], "c"),
                                                      ], "", "false-true (0-1)")

        self.ui.rowInspectorWidget.loadNewData(data)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = Application()
    w.show()
    sys.exit(app.exec_())
